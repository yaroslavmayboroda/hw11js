'use strict';

function togglePassword(inputId, toggleElement) {
    const passwordInput = document.getElementById(inputId);

    if (passwordInput.type === "password") {
        passwordInput.type = "text";
        toggleElement.innerHTML = '<img src="show-password.png" alt="Показати пароль">';
    } else {
        passwordInput.type = "password";
        toggleElement.innerHTML = '<img src="hide-password.png" alt="Сховати пароль">';
    }
}

function checkPassword() {
    const password1 = document.getElementById('password1').value;
    const password2 = document.getElementById('password2').value;

    if (password1 === password2) {
        alert('You are welcome!');
    } else {
        const errorMessage = document.createElement('p');
        errorMessage.textContent = 'Потрібно ввести однакові значення';
        errorMessage.style.color = 'red';
        document.body.appendChild(errorMessage);
    }
}